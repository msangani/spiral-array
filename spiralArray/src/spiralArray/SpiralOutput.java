package spiralArray;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class SpiralOutput {
	
	public final static int endRowIdx = 4;
	public final static int endColIdx = 4;
	
	void spiralPrint(int endRowIdx, int endColIdx, int a[][])
	{
	    int iter, startRowIdx = 0, startColIdx = 0;
	 
	    /*  startRowIdx - starting row index
	        endRowIdx - ending row index
	        startColIdx - starting column index
	        endColIdx - ending column index
	        iter - iterator
	    */
	    
	    while (startRowIdx < endRowIdx && startColIdx < endColIdx)
	    {
	        /* Print the first row from the remaining rows */
	        for (iter = startColIdx; iter < endColIdx; ++iter)
	        {
	            System.out.format("%d ", a[startRowIdx][iter]);
	        }
	        startRowIdx++;
	 
	        /* Print the last column from the remaining columns */
	        for (iter = startRowIdx; iter < endRowIdx; ++iter)
	        {
	        	System.out.format("%d ", a[iter][endColIdx-1]);
	        }
	        endColIdx--;
	 
	        /* Print the last row from the remaining rows */
	        if ( startRowIdx < endRowIdx)
	        {
	            for (iter = endColIdx-1; iter >= startColIdx; --iter)
	            {
	            	System.out.format("%d ", a[endRowIdx-1][iter]);
	            }
	            endRowIdx--;
	        }
	 
	        /* Print the first column from the remaining columns */
	        if (startColIdx < endColIdx)
	        {
	            for (iter = endRowIdx-1; iter >= startRowIdx; --iter)
	            {
	            	System.out.format("%d ", a[iter][startColIdx]);
	            }
	            startColIdx++;    
	        } 
	        
	        
	    }//nd while
	}
	 
	/* main */
	public static void main(String[] args) {
		
		//testing
//	    int a[][] = { {1, 2, 3, 4},
//	        {5, 6, 7, 8 },
//	        {9,  10, 11, 12},
//	        {13, 14, 15, 16}
//	    };
	    
	    SpiralOutput sp = new SpiralOutput();
	    String filePath = args[0];
	    
		int x=0, y=0;
		int rows=0;
		int cols =0;
		//store number of columns from each row in a 2d array
		//using set because it doesn't allow duplicate
		Set<Integer> numCols = new HashSet<Integer>();
		
		ArrayList<String> rowList = new ArrayList<String>();
		try
        {
			//Read file and return the count of rows and columns to calculate 2d array size
			BufferedReader in = new BufferedReader(new FileReader(filePath));	//reading files in specified directory
			String line;
			while ((line = in.readLine()) != null)	//file reading
			{
				//rows count
				rows++;
				rowList.add(line);//add it to arraylist to process later
				
				String[] values = line.split(",");//tokenize to count columns in a row
				
				cols = values.length;
				numCols.add(cols) ;
			}
			
        	in.close();
        	
        	//check if columns in each row are same otherwise throw an exception
        	if( numCols.size() > 1){//found rows that are not fixed in size
        		throw new Exception();
        	}
        	
        	//DEBUG
        	//System.out.println("Rows/Cols: " + rows + ", " + cols);
        	
        	int[][] matrix = new int[rows][cols];
        	
        	//now loop thru array list
        	for (String str : rowList)
        	{
        		//build 2d array by going thru each entry in array list
        		String[] values = str.split(",");
        		for (int i = 0; i < values.length; i++) {
					String string = values[i];
					int str_int = Integer.parseInt(string);
	        		matrix[x][y]=str_int;//store values in 2d array
	        		y=y+1;
				}
        		y=0;//reset to 0 for next line
            	x=x+1;
        	}
        	
        	//call method to perform spiral logic and print the values
        	sp.spiralPrint(rows, cols, matrix);
        	
        	//new line
	        System.out.println();
	        
        }catch( IOException ioException ) {
        	System.out.println(ioException.getStackTrace());
        } catch (Exception e) {
			System.out.println("Column size varies in the given array. \nPlease ensure the column size is same for each row.");
		}
	}


}
